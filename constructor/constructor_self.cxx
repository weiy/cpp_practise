//https://www.geeksforgeeks.org/constructors-c/


// CPP program to illustrate 
// parameterized constructors 
#include <iostream> 
  
class Point { 
private: 
  int x, y; 
  
public: 
  // Parameterized Constructor 
  Point(int x1, int y1 ) { 
    x = x1; 
    y = y1;
    std::cout <<"constructor with parameters called"<<std::endl;
  } 
 
  Point() { 
    int x; 
    int y;
    std::cout <<"constructor without parameters called"<<std::endl;
  } 
  
 
  int getX() 
  { 
    return x; 
  } 
  int getY() 
  { 
    return y; 
  } 
}; 
  
int main() 
{ 
  // Constructor called 
  Point p1(2,3); 
  Point p2();
  // Access values assigned by constructor 
  std::cout << "p1.x = " << p1.getX() << ", p1.y = " << p1.getY()<<std::endl; 
  
  return 0; 
} 
