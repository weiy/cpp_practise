// https://www.geeksforgeeks.org/static-keyword-cpp/
// C++ program to demonstrate 
// the use of static Static 
// variables in a Function 
#include <iostream> 
#include <string> 
using namespace std; 

void demo() 
{ 
  // static variable 
  static int count = 0; 
  cout << count << " "; 
  
  // value is updated and 
  // will be carried to next 
  // function calls 
  count++; 
} 

void demo1()
{
  static int count = 6;
  cout << count << " "; 
  
  // value is updated and 
  // will be carried to next 
  // function calls 
  count++; 
} 
    

int main() 
{ 
  for (int i=0; i<5; i++) 
    {demo(); demo1();}
  return 0; 
} 


// g++ static.cxx -o static
// ./static
// output: 0 6 1 7 2 8 3 9 4 10
