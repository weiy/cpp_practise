//https://stackoverflow.com/questions/23614240/using-operator-with-a-pointer-to-ifstream-object
#include <iostream>     // std::cout                                            
#include <fstream>      // std::ifstream

double read_double(std::ifstream& stream)
{
  double d;
  stream >> d;
  return d;
}

int main()
{
  std::ifstream os("test1.txt");
  double input = read_double(os);
  
  while(os.good()){  
  std::cout << input;
  input = read_double(os);
  }
  return 0;
}
