#include <iostream>
#include <string>


// Base class
class Vehicle {
protected:
    std::string type;
    double speed;
    double fuel;

public:
    Vehicle(const std::string& type, double speed, double fuel)
        : type(type), speed(speed), fuel(fuel) {}

    // Virtual function to display vehicle details
    virtual void displayDetails() const {
        std::cout << "Type: " << type << ", Speed: " << speed << " km/h, Fuel: " << fuel << " L" << std::endl;
    }

    // function to calculate travel time
    virtual double calculateTravelTime(double distance) const {
        return distance / speed; // Time = Distance / Speed
    }

    // Virtual destructor
    virtual ~Vehicle() = default;
};

// Derived class
class Car : public Vehicle {
private:
    int numSeats;

public:
    Car(double speed, double fuel, int numSeats)
        : Vehicle("Car", speed, fuel), numSeats(numSeats) {}

    // Override displayDetails to include number of seats
    void displayDetails() const override {
        Vehicle::displayDetails(); // Reuse base class functionality
        std::cout << "Number of Seats: " << numSeats << std::endl;
    }
};

// Derived class
class Bicycle : public Vehicle {
private:
    bool hasBasket;

public:
    Bicycle(double speed, double fuel, bool hasBasket)
        : Vehicle("Bicycle", speed, fuel), hasBasket(hasBasket) {}

    // Override displayDetails to include basket information
    void displayDetails() const override {
        Vehicle::displayDetails(); // Reuse base class functionality
        std::cout << "Has Basket: " << (hasBasket ? "Yes" : "No") << std::endl;
    }
};

// Derived class
class Truck : public Vehicle {
private:
    double cargoCapacity;

public:
    Truck(double speed, double fuel, double cargoCapacity)
        : Vehicle("Truck", speed, fuel), cargoCapacity(cargoCapacity) {}

    // Override displayDetails to include cargo capacity
    void displayDetails() const override {
        Vehicle::displayDetails(); // Reuse base class functionality
        std::cout << "Cargo Capacity: " << cargoCapacity << " kg" << std::endl;
    }
};

int main() {
    Car car(100, 50, 5);
    Bicycle bicycle(20, 0, true);
    Truck truck(80, 200, 5000);

    // Use base class pointers to demonstrate polymorphism
    Vehicle* vehicles[] = { &car, &bicycle, &truck };

    for (const auto& vehicle : vehicles) {
        vehicle->displayDetails();
        std::cout << "Travel Time for 100 km: " << vehicle->calculateTravelTime(100) << " hours\n\n";
    }

    return 0;
}


/*
[weiy@localhost override]$ g++ -o main main.cxx 
[weiy@localhost override]$ ./main 
Type: Car, Speed: 100 km/h, Fuel: 50 L
Number of Seats: 5
Travel Time for 100 km: 1 hours

Type: Bicycle, Speed: 20 km/h, Fuel: 0 L
Has Basket: Yes
Travel Time for 100 km: 5 hours

Type: Truck, Speed: 80 km/h, Fuel: 200 L
Cargo Capacity: 5000 kg
Travel Time for 100 km: 1.25 hours
*/
