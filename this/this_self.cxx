//https://www.tutorialspoint.com/cplusplus/cpp_this_pointer.htm

#include <iostream>

class Box {
public:
  // Constructor definition
  Box(double l = 2.0, double b = 2.0, double h = 2.0) {
    std::cout <<"Constructor called." << std::endl;
    length = l;
    breadth = b;
    height = h;
  }
  double Volume() {
    return length * breadth * height;
  }
  int compare(Box box) {
    return this->Volume() > box.Volume();
  }
      
private:
  double length;     // Length of a box
  double breadth;    // Breadth of a box
  double height;     // Height of a box
};

int main(void) {
  Box Box1(2,3,4);    // Declare box1
  Box Box2(5, 6, 7);    // Declare box2
  
  if(Box1.compare(Box2)) {
    std::cout << "Box2 is smaller than Box1" <<std::endl;
  } else {
    std::cout << "Box2 is equal to or larger than Box1" <<std::endl;
  }

  return 0;
}

/*

[weiy@pplxint11 this]$ g++ this_self.cxx -o this_self
[weiy@pplxint11 this]$ ./this_self 
Constructor called.
Constructor called.
Box2 is equal to or larger than Box1

*/
