#include <ctime>
#include <iostream>
#include <time.h>
#include <thread>
#include <chrono>
//g++ time_self.cxx -o time_self -std=c++11
using namespace std;
int main()
{
  time_t current_time;
  // Stores time in current_time
  time(&current_time);
  cout << current_time << " seconds has passed since 00:00:00 GMT, Jan 1, 1970"<<endl;

  clock_t t;
  t = clock();
  cout <<"t"<<(double)t<<endl;
  /*program*/
  std::cout << "countdown:\n";
  for (int i=10; i>0; --i) {
    std::cout << i << std::endl;
    std::this_thread::sleep_for (std::chrono::seconds(1));
  }
  std::cout << "Lift off!\n";
  t = clock() - t;
  int total_time1 = 0;
  total_time1 += t;
  cout<<"t"<<(double)t<<endl;
  cout << "total_time1 in milliseconds:" << 1.0*total_time1/CLOCKS_PER_SEC*1000<<endl;
  //CLOCKS_PER_SEC is 1,000,000 

  return 0;
}
