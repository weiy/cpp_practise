//https://www.learncpp.com/cpp-tutorial/610-pointers-and-const/
#include <iostream>

int main(){

  int value = 5;
  //const int value = 5; // which is not allowed to change the value
  int *ptr = &value;
  *ptr = 6; // change value to 6
  std::cout << value;


  const int value2 = 10;
  //value2 = 10; // not allowed (even) during compile session  
}
