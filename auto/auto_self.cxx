//https://www.geeksforgeeks.org/type-inference-in-c-auto-and-decltype/

#include <bits/stdc++.h> 
using namespace std; 
  
int main() 
{ 
  auto x = 4; 
  auto y = 3.37; 
  auto ptr = &x; 
  cout << typeid(x).name() << endl 
       << typeid(y).name() << endl 
       << typeid(ptr).name() << endl; 
  
  return 0; 
} 

//g++ auto_self.cxx -o auto_self -std=c++11
